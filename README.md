# Setting Up Project

## NPM
* Run `npm install` to install packages.
* Run `npm start` to start working on the project.
* Run `npm run dev` to get development files.
* Run `npm run build` to build files for production.
