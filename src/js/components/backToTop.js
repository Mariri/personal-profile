/**
 * Shows back to top on scroll 
 *  and add back to top functionality
 */

const backToTop = () => {
  const backTop = document.querySelector('.floating__top')

  backTop.addEventListener('click', () => {
    document.body.scrollIntoView({ behavior: 'smooth' });
  })

  if(window.pageYOffset >= 500) {
    backTop.classList.add('floating__top--active');
  } else {
    backTop.classList.remove('floating__top--active');
  }

  window.addEventListener('scroll', () => {
    if(window.pageYOffset >= 500) {
      backTop.classList.add('floating__top--active');
    } else {
      backTop.classList.remove('floating__top--active');
    }
  })
}

export default backToTop
