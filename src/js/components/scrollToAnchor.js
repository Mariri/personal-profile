/**
 * Scroll to anchor on click
 */

const scrollToAnchor = () => {
  const anchorLink = document.querySelectorAll("a[href^='#']");
  const navigation = document.querySelector('.navigation');

  for(let item of anchorLink) {
    item.addEventListener('click', (e) => {
      e.preventDefault()
      document.querySelector(item.getAttribute('href')).scrollIntoView({ behavior: 'smooth' })
      navigation.classList.remove('navigation--active')
    })
  }
}

export default scrollToAnchor
