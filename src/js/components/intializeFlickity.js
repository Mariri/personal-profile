/**
 * Initializes flickity
 */

const initializeFlickity = () => {
  const carousel = document.querySelector('.carousel')
  new Flickity(carousel, {
    wrapAround: true,
    autoPlay: true,
    adaptiveHeight: false,
    lazyLoad: true,
    prevNextButtons: false
  })
}

export default initializeFlickity
