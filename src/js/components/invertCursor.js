/**
 * Inverts the color of the cursor
 */

const invertCursor = () => {
  const cursor = document.querySelector('.cursor') || null;
  const cursorInner = cursor.querySelector('.cursor--inner') || null;
  let clientX = -100;
  let clientY = -100;

  if(window.innerWidth >= 768) {
    document.addEventListener('mousemove', function(e) {
      clientX = e.clientX;
      clientY = e.clientY;

      if (e.target.tagName === 'a' || e.target.tagName === 'A' || e.target.closest('A') || e.target.tagName === 'BUTTON' || e.target.classList.contains('carousel__cell')) {
        cursorInner.classList.add('cursor--on-link');
      } else {
        cursorInner.classList.remove('cursor--on-link');
      }

      if (e.target.tagName === 'IFRAME') {
        cursorInner.classList.add('d-none');
      } else {
        cursorInner.classList.remove('d-none');
      }
    });

    function render() {
      cursor.style.transform = `translate(${clientX - 5}px, ${clientY - 5}px)`;

      requestAnimationFrame(render);
    }
    requestAnimationFrame(render);
  }
}

export default invertCursor
