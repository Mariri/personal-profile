/**
 * Toggles the navigation
 */

const toggleNavigation = () => {
  const menu = document.querySelector('.navigation__menu')
  const navigation = document.querySelector('.navigation')

  menu.addEventListener('click', () => {
    navigation.classList.toggle('navigation--active')
  })
}

export default toggleNavigation
