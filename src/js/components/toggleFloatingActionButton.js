/**
 * Toggles the floating action button on click
 */

const toggleFloatingActionButton = () => {
  const fab = document.querySelector('.floating__button')
  const floatingContact = document.querySelector('.floating__contact')

  fab.addEventListener('click', () => {
    floatingContact.classList.toggle('floating__contact--active')
  })
}

export default toggleFloatingActionButton
