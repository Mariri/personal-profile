import toggleNavigation from './components/toggleNavigation'
import intializeFlickity from './components/intializeFlickity'
import toggleFloatingActionButton from './components/toggleFloatingActionButton'
import backToTop from './components/backToTop'
import scrollToAnchor from './components/scrollToAnchor'
import invertCursor from './components/invertCursor'

document.addEventListener(
  'DOMContentLoaded',
  () => {
    toggleNavigation()
    intializeFlickity()
    toggleFloatingActionButton()
    backToTop()
    scrollToAnchor()
    invertCursor()
  },
  false
)
